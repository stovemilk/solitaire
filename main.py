from random import shuffle
import sys
import os
import getch

# Colors to be used throughout game
colors = {
    "normal": '\033[97m',
    "black": '\033[90m',
    "red": '\033[91m',
    "none": "\033[40m",
    "selected": "\033[107m",
}

# List of all four suits
suits = ["spades", "hearts", "clubs", "diamonds"]

# Card object
class Card:
    def __init__(self, number, suit, color, hidden = True, highlight = "none", pile = None):
        self.number = number
        self.suit = suit
        self.color = color
        self.hidden = hidden
        self.highlight = highlight
        self.pile = pile
        
        if self.hidden == True:
            self.hide()
        elif self.hidden == False:
            self.show()

    def show(self, highlight = "none"):
        self.value = colors[highlight] + colors[self.color] + str("%02d"%self.number) + self.suit[0] + colors["normal"] + colors["none"]
        self.hidden = False

    def hide(self):
        self.value = "###"
        self.hidden = True
        return self 

# Playing field
class Field:
    # Scoring piles
    scorePiles = {
        "spades": [],
        "hearts": [],
        "clubs": [],
        "diamonds": []
    }
  
    # Piles to draw from and move to the play area from
    pickPile = []
    drawPile = []
    
    # List of each row of cards
    playPiles = {
        1: [], 
        2: [], 
        3: [], 
        4: [], 
        5: [], 
        6: [], 
        7: []
    }

    def render(self):
        # Display score pile
        print()
        
        for i in suits:
            try:
                print(colors[self.scorePiles[i][-1].highlight] + self.scorePiles[i][-1].value, end = ' ')
            except IndexError:
                print("...", end = ' ')
        
        print('   ', end = ' ')
        
        # Display draw piles
        try:
            print(self.pickPile[-1].value, end = ' ')
        except IndexError:
            print("...", end = ' ')
        
        try:
            print(self.drawPile[-1].value, end = ' ')
        except IndexError:
            print("...", end = ' ')
        
        print()
        print("-1- -2- -3- -4- -5- -6- -7- ")
        
        # Displays each row one by one
        for i in range(len(max(self.playPiles.values(), key = len))):
            for j in self.playPiles.values():
                try:
                    print(j[i].value, end = ' ')
                # Print blank space if nothing is there
                except IndexError:
                    print("   ", end = ' ')
            else:
                print()
        
        print("\n" * (os.get_terminal_size().lines - (5 + len(max(self.playPiles.values(), key = len)))))

# Cursor object
class Cursor:
    selected = []

    def select(self, card):
        if card.hidden == True:
            return 1
        
        while len(self.selected) > 0:
            self.selected[-1].show()
            self.selected.pop()
        
        selection = []
        
        selection.append(card)
        card.show("selected")
        if card.pile.index(card) < (len(card.pile) - 1):
            for i in range(card.pile.index(card) + 1, len(card.pile)):
                selection.append(card.pile[i])
                card.pile[i].show("selected")
        
        self.selected = selection

class Game:
    solved = False

    # Generate 52 cards in draw pile
    def genDrawPile(self):
        # Makes cards 1 through 13 for each suit
        for i in range(1, 14):
            for j in suits:
                field.drawPile.append(Card(i, j, "black" if j == "spades" or j == "clubs" else "red"))
                field.drawPile[-1].pile = field.drawPile
        
        shuffle(field.drawPile)

    # Draw cards onto field
    def dealCards(self):
        # Move cards to play field
        for i in field.playPiles.keys():
            for j in range(i): 
                field.playPiles[i].append(field.drawPile[-1])
                field.playPiles[i][-1].pile = field.playPiles[i]
                field.drawPile.pop()

        # Unhide top cards
        for i in field.playPiles.values():
            i[-1].show()

    def draw(self):
        if len(field.drawPile) > 0:
            field.pickPile.append(field.drawPile[-1])
            field.pickPile[-1].show()
            field.pickPile[-1].pile = field.pickPile
            field.drawPile.pop()
            cursor.select(field.pickPile[-1])
        else:
            for i in list(reversed(field.pickPile)):
                i.hide()
                i.pile = field.drawPile
                field.drawPile.append(i)
                field.pickPile.pop()

    def move(self, toMove):
        destInput = getch.getche()
        while destInput.isdigit() == False or int(destInput) < 1 or int(destInput) > 7: 
            print("\033[41m" + "Out of range" + "\033[40m")
            destInput = getch.getche()
        destination = field.playPiles[int(destInput)]
       
        prevPile = toMove[0].pile
        if toMove[0].number == 13 and len(destination) == 0 or toMove[0].number == (destination[-1].number - 1) and toMove[0].color != destination[-1].color:
            destination.extend(toMove)
            for i in toMove:
                i.pile.remove(i)
                i.pile = field.playPiles[int(destInput)]
            if len(prevPile) > 0:
                prevPile[-1].show()
        else:
            print("\033[41m" + "Cannot move card" + "\033[40m")

    def score(self):
        card = cursor.selected[-1]
        try:
            if card.hidden == True:
                return 1
            elif card.number == 1 or card.number == (field.scorePiles[card.suit][-1].number + 1):   
                card.show()
                field.scorePiles[card.suit].append(card)
                card.pile.pop()
                card.pile[-1].show()
                cursor.select(card.pile[-1])
                card.pile = field.scorePiles[card.suit]
            else:
                print("\033[41m" + "Cannot score card " + str(card.number) + "\033[40m")
        except IndexError:
            print("\033[41m" + "Failed to score card " + str(card.number) + "\033[40m")
#
# The actual game itself
#

cursor = Cursor()
field = Field()
game = Game()

def main():
    # Initialize the game
    game.genDrawPile()
    game.dealCards()
    cursor.select(field.playPiles[7][6])
    field.render()

    while game.solved == False:
        char = getch.getche()
        
        if char.isdigit() and int(char) > 0 and int(char) < 8:
            cursor.select(field.playPiles[int(char)][-1])
        elif char == "+":
            cursor.select(cursor.selected[0].pile[cursor.selected[0].pile.index(cursor.selected[0]) - 1])
        elif char == "-":
            cursor.select(cursor.selected[0].pile[cursor.selected[0].pile.index(cursor.selected[0]) + 1])
        elif char == "p":
            cursor.select(field.pickPile[-1])
        elif char == "d" or char == " ":
            game.draw()
        elif char == "m":
            print()
            game.move(cursor.selected)
        elif char == "s":
            game.score()
        elif char == "w" and len(field.scorePiles["spades"]) > 0:
            cursor.select(field.scorePiles["spades"][-1])
        elif char == "e" and len(field.scorePiles["hearts"]) > 0:
            cursor.select(field.scorePiles["hearts"][-1])
        elif char == "r" and len(field.scorePiles["clubs"]) > 0:
            cursor.select(field.scorePiles["clubs"][-1])
        elif char == "t" and len(field.scorePiles["diamonds"]) > 0:
            cursor.select(field.scorePiles["diamonds"][-1])
        elif char == "q":
            sys.exit()
        else:
            print("\033[41m" + "Invalid action" + "\033[40m")
        
        for i in field.scorePiles.values():
            if len(i) == 13:
                if i[-1].number != 13:
                    print(i, game.solved)
                    game.solved = False
                    break
                else:
                    print("{} is 13".format(i))
                    game.solved = True
            else:
                break
        
        field.render()
    else:
        print(game.solved)

if __name__ == '__main__':
    main()
